#!/bin/bash
REPACKAGE=false
INT=false
SYS="no"
REBUILD=false
DIR=no

function usage {
        echo "This tool lets you run Librsvg's test suite under a couple different"
        echo "docker containers for testing, it could be adapted to toolbox"
        echo "Use -d [DIRECTORY] pointing at your librsvg directory"
        echo "Use -s [SYSTEM] to determine what docker container to use (Fedora or OpenSUSE)"
        echo "use -h to return this help"
        echo "use -i to have it pause periodically to check output"
        echo "use -r to rebuild the docker image forcefully"
        echo "use -p to repackage the librsvg image (use in conjunction with -r otherwise the cache will stop changes from taking"
}

function package_librsvg {
	if [[ $REPACKAGE == true ]]
	then
		if [[ $INT == true ]]
		then
			read -p "Making a copy, then running make clean and packaging Librsvg, press any key to continue" -n1 -s
		fi
		mkdir /tmp/librsvg
		cp -r $LIBDIR/. /tmp/librsvg
		cd /tmp/librsvg
		./autogen.sh
		make clean
		cd $DIR
		tar -cvzf $SYS/librsvg.tar.gz -C /tmp/librsvg . --xform='s!^\./!!'
	elif [[ -f "$SYS/librsvg.tar.gz" ]]
	then
		echo "Librsvg Directory already packaged"
	fi
}

function build_docker {
	if [[ $REBUILD == true ]]
	then
		if [[ $INT == true ]]
		then
			read -p "Rebuilding Docker with settings $SYS, $LIBDIR Press any key to continue" -n1 -s
		fi
		rebuild_docker
		exit 1
	fi
	if [[ $INT == true ]]
	then
		read -p "Building Docker with cache and settings $SYS, $LIBDIR Press any key to continue" -n1 -s
	fi
	sudo docker build -t madds/librsvg-base-$SYS -f librsvg-base/$SYS/Dockerfile librsvg-base/$SYS/.	
	sudo docker build -t madds/librsvg-$SYS -f $SYS/Dockerfile $SYS/.
	
	sudo docker run -it madds/librsvg-$SYS
	exit 1
}

function rebuild_docker {
	echo "removing old image"
	sudo docker rmi sudo docker rmi madds/librsvg-opensuse
	sudo docker build -t madds/librsvg-$SYS --no-cache -f $SYS/Dockerfile $SYS/.
	sudo docker run -it madds/librsvg-$SYS
	exit 1
}

function check_dir {
	echo "Checking if $LIBDIR exists"
	if [[ ! -d "$LIBDIR" ]]
	then
		echo "$LIBDIR does not exist, exiting"
		exit 2
	fi
	
	if [[ $LIBDIR == */ ]]
	then
		echo "Directory is good!"
	else
		echo "Directory missing last /, adding"
		LIBDIR+="/"
	fi
	
	DIR=$PWD
}

function check_system {
	echo "Checking what system $SYSTEM is"
	if [[ $SYSTEM == "fedora" ]]
	then
		echo "Fedora"
		SYS="fedora"
	elif [[ $SYSTEM == "Fedora" ]]
	then
		echo "Fedora"
		SYS="fedora"
	elif [[ $SYSTEM == "opensuse" ]]
	then
		echo "OpenSUSE"
		SYS="opensuse"
	elif [[ $SYSTEM == "OpenSUSE" ]]
	then
		echo "OpenSUSE"
		SYS="opensuse"
	else 
		echo "Wrong System selected, must be fedora or opensuse"
		echo $flag
		exit 2
	fi
}

if [[ ${#} -eq 0 ]]; then
   usage
   exit 2
fi

while getopts "d:s:irph" flag; do
	case "${flag}" in
		d) LIBDIR=${OPTARG};;
		s) SYSTEM=${OPTARG};;
		i) INT=true;;
		r) REBUILD=true; echo "Rebuilding";;
		p) REPACKAGE=true; echo "Repackaging";;
		h) usage; echo "hm $flag"; exit 1;;
		?) usage; echo "hmbad $flag"; exit 2;;
	esac
done

function main {
	check_dir
	check_system
	echo $INT
	echo $SYS
	echo $REPACKAGE
	echo $REBUILD
	package_librsvg
	build_docker
	rm -r /tmp/librsvg
}
main
